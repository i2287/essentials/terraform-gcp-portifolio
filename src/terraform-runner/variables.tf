variable "machine" {
  type    = string
  default = "e2-micro"
}

variable "machine-name" {
  type    = string
  default = "runner-lab-runner"
}

variable "tags" {
  type    = list(string)
  default = ["lab","q3","tags","runner"]
}

variable "labels" {
  type    = map(string)
    default = {
    "env" = "lab"
    "quarter" = "q3"
    "variablename" = "tags-network"
    "function" = "runner"
    "track" = "1"
        }   
}

variable "ssh-key" {
    type = list(string)
    default = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwrsAkj/pSmc8VlFjFzFt7D0eK104GGxTdQA1V1FWzqKyAtxyXCO2NtQluRZeMaWHisAPLGezCtNacyZ5+gY6pJso3yDtBNw/kWXgFQnCUU27v9E5q0kHhFS/LOa6iVTSNSPAKTSsxb/6DeMmBfL9tC8wUn+pUYHRs4rY+QIbxgLE7OuXCEXmeXxC50yZstOmDG5eW4btWmeAmp2AHtaeZRqkv+IJr/YIgTGoIwoTom/4EdjyT0muCJ8FyLYsUYIfn3sN1iXkx3Vr3mw/XD0cmaseqz7Pa4LE6CF6gQ/oXiWm9aTWuilaRV5wx5JIgGoFTRy+eX6oenTlCUjoNDPDWNVBL1DAPdE6oyD4Lc2eWHyUV6lt1bXhkOhYvYTKK97WNTXTqnV8NRic5kdqD5lsoQUgzyr3TcM8udz8d+d51v0KjFVS+QjQibeDbXLpPPtNlSKETe/9pzQTi57jta2hZ1aF2rOyG4fidY26fjJjoflGhEBRLZ/Jsn6IIfF/oT/c= rodrigo.silva.cunha@C02F130PML7H",
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDUEqv31caH2eUxaJs/dCaqzSwqsGfGJh9eI8I8WA3ZhVK2jGjrqFAnySFIaE3QNfaebN0HP8Pkfn4TQDLCTaa+0CEBC7pOG7IqxJl2BitIi0bRaUbrcAPfEceG245lC0X4iqhP+LyhTzcUMCLiYXKOB5vsiuB69XBc8z4yMN/GKrVJpV0exehUQDBzBvQ7t/dq4oUcFixLHXS79ILcdbKDsVB/YMT5tu353Y9BT7tUUJz/aSbW3hSxqfi0Vrl3UEtKAx6RIQa2pHVPFxNrcIMpcHlB925R/a/ruzzMXsZJMjF2GkXQiEK6Tdj+UNljKksIQXHf5uUFfMjwJRBJb9TTA2itBtX7hifORlouYxW2qNwkJK/PRBmVePB5bR6pzV+f2xF+y2theCcNn6nwjXktwl1DL+p1i4pVfpPmE8EDA7aVWszfHPZ2acbOGAe4RdJORnrWFi6DNHtLDlUQi9CTttjmlUJMUjBVB2FPxATsteXI4b1cw9PLr13QnaEfnk8= rcunha@rcunha-System-Product-Name"
    ]
}

# variable "so-user" {
#   type    = string
#   default = "rodrigo_root_rj"
# }

variable "so-user" {
type = list(string)
    default = ["ansible","rcunha","rodrigo_root_rj"]
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("keys/key.json")

  project = "iac-q32023"
  region  = "us-central1"
  zone    = "us-central1-a"
}
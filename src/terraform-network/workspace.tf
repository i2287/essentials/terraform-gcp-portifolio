terraform {
  backend "gcs" {
    bucket  = "terraform-state-template-vm"
    prefix  = "terraform-network/state"
  }
}
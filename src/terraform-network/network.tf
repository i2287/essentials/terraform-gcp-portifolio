resource "google_compute_firewall" "firewall-rule" {
  name    = "network-lab-firewall-b-docker-web"
  network = "network-compute-b"
    description = "Creates firewall rule targeting tagged instances"


  allow {
    protocol = "tcp"
    ports    = ["9091"]
  }

  source_tags = ["docker-tcp-9091"]
}
resource "google_compute_network" "network-compute" {
  name = "network-lab-c"
  auto_create_subnetworks = true
  mtu                     = 1460
}
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("keys/key.json")

  project = "iac-q32023"
  region  = "us-central1"
  zone    = "us-central1-a"
}


//# RESOURCES #
resource "google_compute_instance" "default" {
  name         = var.machine-name
  machine_type = "f1-micro"
  zone         = "us-central1-b"

  tags = var.tags
  labels = var.labels

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  network_interface {
    network = "network-lab"

    access_config {
      // Ephemeral public IP
    }
  }
  metadata_startup_script = "apt-get update -y && apt-get install nginx -y"
 
}
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("keys/key.json")

  project = "iac-q32023"
  region  = var.region
  zone    = var.zone
}


//# RESOURCES #
resource "google_compute_instance" "default" {
  name         = var.machine-name
  machine_type = var.machine
  zone         =  var.zone

  tags = var.tags
  labels = var.labels

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  // Local SSD disk
  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
  metadata_startup_script = "curl -L 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh' | sudo bash &&  apt-get update -y && apt-get install gitlab-runner -y"


  metadata = {
    sshKeys = "${var.so-user[0]}:${var.ssh-key[0]} \n ${var.so-user[1]}:${var.ssh-key[1]} \n ${var.so-user[2]}:${var.ssh-key[1]}"   
  }

}
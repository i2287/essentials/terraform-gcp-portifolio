terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("keys/key.json")

  project = "iac-q32023"
  region  = var.region
  zone    = var.zone
}


//# RESOURCES #
resource "google_compute_instance" "default" {
  name         = var.machine-name
  machine_type = var.machine
  zone         =  var.zone

  tags = var.tags
  labels = var.labels

  boot_disk {
    initialize_params {
      image = "ubuntu-2204-jammy-v20230919"
      labels = {
        my_label = "value"
      }
    }
  }

  // Local SSD disk
  network_interface {
    network = "network-lab-b"

    access_config {
      // Ephemeral public IP
    }
  }
  metadata_startup_script = "curl -L 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh' | sudo bash &&  apt-get update -y && apt-get install gitlab-runner -y"


  metadata = {
    sshKeys = "${var.so-user}:${var.ssh-key[0]} \n ${var.so-user}:${var.ssh-key[1]} \n  ${var.so-ansible-user}:${var.ssh-key[1]}  \n ${var.so-rcunha-user}:${var.ssh-key[1]}"
  }

}
variable "gke_username" {
  default     = "user"
  description = "gke username"
}

variable "gke_password" {
  default     = "pass"
  description = "gke password"
}

variable "gke_num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "region" {
  type    = string
  default = "asia-east1"
}

variable "zone" {
  type    = string
  default = "asia-east1-a"
}

variable "project_id" {
  description = "iac-q32023"
}
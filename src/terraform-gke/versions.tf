# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

terraform {
  backend "gcs" {
    bucket  = "terraform-state-template-vm"
    prefix  = "terraform-runner/state"
  }
}


version:=$(shell cat version)
project=rodrigorootrj/terraform-gcp-essentials
TAG=${project}:${version}
TAG-SEED=${project}:${version}-seed


echo:
	@echo  ${TAG}
	@echo ${TAG-SEED}
build-seed:
	@docker build -f $$PWD/seed/Dockerfile -t ${TAG-SEED} .
build:
	@docker build . -t ${TAG}
run: build
	@docker run -it -v $$PWD/app/:/app  -v $$PWD/keys/:/keys ${TAG}
#run, com variaveis de ambiente.
run_variables: build
	@docker run -it -v $$PWD/app/:/app  -v $$PWD/keys/:/keys  -e GCP_KEY=${GCP_KEY} -e GCP_PROJECT=${GCP_PROJECT} ${TAG}	
push-seed: build-seed
	@docker push ${TAG-SEED}
push: build
	@docker push ${TAG}	
#ts docker hub
docker_remove:
	@docker rmi ${TAG} --force
docker_run_hub:
	@docker run -it ${TAG}